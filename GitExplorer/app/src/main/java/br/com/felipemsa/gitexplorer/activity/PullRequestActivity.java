package br.com.felipemsa.gitexplorer.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.felipemsa.gitexplorer.R;
import br.com.felipemsa.gitexplorer.adapter.PullRequestAdapter;
import br.com.felipemsa.gitexplorer.helper.EndlessScrollListener;
import br.com.felipemsa.gitexplorer.model.PullRequest;
import br.com.felipemsa.gitexplorer.model.Repository;
import br.com.felipemsa.gitexplorer.repository.Git;
import br.com.felipemsa.gitexplorer.repository.RepositoryDatabase;
import cz.msebera.android.httpclient.Header;

/**
 * Created by felipealmeida on 25/10/15.
 */
public class PullRequestActivity extends AppCompatActivity {

    private Git git = Git.getInstance();
    private Gson gson = new Gson();

    private ListView list;
    private PullRequestAdapter adapter;
    private ArrayList<PullRequest> pullRequests = new ArrayList();

    private Repository repo;
    private RepositoryDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        db = new RepositoryDatabase(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        repo = gson.fromJson(getIntent().getStringExtra("repository"), Repository.class);

        list = (ListView) findViewById(R.id.list);
        adapter = new PullRequestAdapter(this, pullRequests);
        list.setAdapter(adapter);

        list.setOnScrollListener(new EndlessScrollListener(10, 1) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                load(page);
                return true;
            }
        });

        load(1);
    }

    private void load(int page) {
        git.getRepositoryPullRequests(repo, page, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    for (int i = 0; i < array.length(); i++)
                        pullRequests.add(gson.fromJson(array.getJSONObject(i).toString(), PullRequest.class));

                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    Toast.makeText(PullRequestActivity.this, "Ops!\nAlgo não está funcionando bem...\nTente novamente em alguns segundos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.d("ERROR", "statusCode: " + statusCode
                        + "\nheaders: " + headers
                        + "\nresponse: " + errorResponse);

                Toast.makeText(PullRequestActivity.this, "No momento não há mais itens a exibir...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                findViewById(R.id.pd).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }
}
