package br.com.felipemsa.gitexplorer.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import br.com.felipemsa.gitexplorer.repository.interfaces.GitRepositoriesInterface;

/**
 * Created by felipealmeida on 24/10/15.
 */
public class Repository {

    private long id;
    private Owner owner;
    private String name;
    private String description;
    private long forks;
    @SerializedName("stargazers_count")
    private long stars;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getForks() {
        return forks;
    }

    public void setForks(long forks) {
        this.forks = forks;
    }

    public long getStars() {
        return stars;
    }

    public void setStars(long stars) {
        this.stars = stars;
    }

    public ContentValues values() {
        ContentValues values = new ContentValues();

        values.put(GitRepositoriesInterface.REPOSITORY_COLUMN_ID, getId());
        values.put(GitRepositoriesInterface.REPOSITORY_COLUMN_NAME, getName());
        values.put(GitRepositoriesInterface.REPOSITORY_COLUMN_DESCRIPTION, getDescription());
        values.put(GitRepositoriesInterface.REPOSITORY_COLUMN_FORKS, getForks());
        values.put(GitRepositoriesInterface.REPOSITORY_COLUMN_STARTS, getStars());
        values.put(GitRepositoriesInterface.REPOSITORY_COLUMN_OWNER_ID, getOwner().getId());

        return values;
    }
}
