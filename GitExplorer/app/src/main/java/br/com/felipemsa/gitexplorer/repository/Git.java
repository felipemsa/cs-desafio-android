package br.com.felipemsa.gitexplorer.repository;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import br.com.felipemsa.gitexplorer.model.Repository;

/**
 * Created by felipealmeida on 25/10/15.
 */
public class Git {

    private static Git git;

    private static final String GIT_REPOSITORY_URL = "https://api.github.com/search/repositories";
    private static final String GIT_PULL_REQUESTS_URL = "https://api.github.com/repos/%s/%s/pulls";

    private static AsyncHttpClient client;

    private Git() {
        client = new AsyncHttpClient();
        client.setUserAgent("felipemsa");
    }

    public static Git getInstance() {
        if (git == null)
            git = new Git();

        return git;
    }

    public static void getRepositoryList(int page, JsonHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.add("access_token", "0dbc57255f2a78591d137e34394d7ead33714891");
        params.add("q", "language:Java");
        params.add("sort", "start");
        params.add("page", String.valueOf(page));

        client.get(GIT_REPOSITORY_URL, params, responseHandler);
    }

    public static void getRepositoryPullRequests(Repository repo, int page, JsonHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.add("access_token", "0dbc57255f2a78591d137e34394d7ead33714891");
        params.add("page", String.valueOf(page));

        client.get(String.format(GIT_PULL_REQUESTS_URL, repo.getOwner().getName(), repo.getName()), params, responseHandler);
    }
}
