package br.com.felipemsa.gitexplorer.repository.interfaces;

/**
 * Created by felipealmeida on 28/10/15.
 */
public interface GitRepositoriesInterface {

    // Usuários
    public static final String OWNER_TABLE_NAME = "owner";

    public static final String OWNER_COLUMN_ID = "o_id";
    public static final String OWNER_COLUMN_NAME = "o_name";
    public static final String OWNER_COLUMN_PHOTO = "o_photo";

    public static final String OWNER_CREATE_TABLE = "CREATE TABLE " + OWNER_TABLE_NAME + " ( " + OWNER_COLUMN_ID + " INTEGER PRIMARY KEY, " +
            OWNER_COLUMN_NAME + " TEXT, " + OWNER_COLUMN_PHOTO + " TEXT )";

    // Repositórios
    public static final String REPOSITORY_TABLE_NAME = "repository";

    public static final String REPOSITORY_COLUMN_ID = "r_id";
    public static final String REPOSITORY_COLUMN_NAME = "r_name";
    public static final String REPOSITORY_COLUMN_DESCRIPTION = "r_description";
    public static final String REPOSITORY_COLUMN_FORKS = "r_forks";
    public static final String REPOSITORY_COLUMN_STARTS = "r_stars";
    public static final String REPOSITORY_COLUMN_OWNER_ID = "r_owner_id";

    public static final String REPOSITORY_CREATE_TABLE = "CREATE TABLE " + REPOSITORY_TABLE_NAME + " ( " + REPOSITORY_COLUMN_ID + " INTEGER PRIMARY KEY, " +
            REPOSITORY_COLUMN_NAME + " TEXT, " + REPOSITORY_COLUMN_DESCRIPTION + " TEXT, " + REPOSITORY_COLUMN_FORKS + " INTEGER, " +
            REPOSITORY_COLUMN_STARTS + " INTEGER, " + REPOSITORY_COLUMN_OWNER_ID + " INTEGER, FOREIGN KEY (" + REPOSITORY_COLUMN_OWNER_ID + ") " +
            "REFERENCES " + OWNER_TABLE_NAME + " (" + OWNER_COLUMN_ID + ") " + ")";

    // Pull Requests
    public static final String PULL_REQUEST_TABLE_NAME = "pull_requests";

    public static final String PULL_REQUEST_COLUMN_ID = "p_id";
    public static final String PULL_REQUEST_COLUMN_TITLE = "p_title";
    public static final String PULL_REQUEST_COLUMN_BODY = "p_body";
    public static final String PULL_REQUEST_COLUMN_OWNER_ID = "p_owner_id";

    public static final String PULL_REQUEST_CREATE_TABLE = "CREATE TABLE " + PULL_REQUEST_TABLE_NAME + " ( " + PULL_REQUEST_COLUMN_ID + " INTEGER PRIMARY KEY, " +
            PULL_REQUEST_COLUMN_TITLE + " TEXT, " + PULL_REQUEST_COLUMN_BODY + " TEXT, " + PULL_REQUEST_COLUMN_OWNER_ID + " INTEGER, FOREIGN KEY (" +
            PULL_REQUEST_COLUMN_OWNER_ID + ") " + "REFERENCES " + OWNER_TABLE_NAME + " (" + OWNER_COLUMN_ID + ") " + ")";
}
