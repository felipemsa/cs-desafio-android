package br.com.felipemsa.gitexplorer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.felipemsa.gitexplorer.R;
import br.com.felipemsa.gitexplorer.helper.RoundedTransformation;
import br.com.felipemsa.gitexplorer.model.PullRequest;

/**
 * Created by felipealmeida on 25/10/15.
 */
public class PullRequestAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<PullRequest> pullRequests;

    public PullRequestAdapter(Context context, ArrayList<PullRequest> listPullRequests) {
        mContext = context;
        pullRequests = listPullRequests;
    }

    @Override
    public int getCount() {
        return pullRequests.size();
    }

    @Override
    public PullRequest getItem(int position) {
        return pullRequests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null)
            view = LayoutInflater.from(mContext).inflate(R.layout.list_pull_item, null);

        TextView tvPullTitle = (TextView) view.findViewById(R.id.tvPullTitle);
        TextView tvPullBody = (TextView) view.findViewById(R.id.tvPullBody);
        ImageView ivOwnerPhoto = (ImageView) view.findViewById(R.id.ivOwnerPhoto);
        TextView tvOwnerName = (TextView) view.findViewById(R.id.tvOwnerName);

        PullRequest pull = getItem(position);

        tvPullTitle.setText(pull.getTitle());
        tvPullBody.setText(pull.getBody());
        tvOwnerName.setText(pull.getOwner().getName());

        Picasso.with(mContext).load(pull.getOwner().getPhoto()).transform(new RoundedTransformation(200, 5)).fit().into(ivOwnerPhoto);

        return view;
    }
}
