package br.com.felipemsa.gitexplorer.repository;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import br.com.felipemsa.gitexplorer.model.Owner;
import br.com.felipemsa.gitexplorer.model.PullRequest;
import br.com.felipemsa.gitexplorer.model.Repository;
import br.com.felipemsa.gitexplorer.repository.interfaces.GitRepositoriesInterface;

/**
 * Created by felipealmeida on 27/10/15.
 */
public class RepositoryDatabase implements GitRepositoriesInterface {

    private static final String DATABASE_NAME = "git-explorer";

    private static final int DATABASE_VERSION = 1;

    private static final String TAG_DGE = "DB EXCEPTION";

    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;

    private Context mContext;

    public RepositoryDatabase(Context ctx) {
        mContext = ctx;

        dbHelper = new DatabaseHelper(mContext);
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        if (database.isOpen())
            database.close();
    }

    public ArrayList<Repository> getRepositories(int limit, int offset) {
        ArrayList<Repository> repositories = new ArrayList();

        String query = "SELECT r.*, o.* FROM " +
                REPOSITORY_TABLE_NAME + " r, " +
                OWNER_TABLE_NAME + " o WHERE " +
                "r." + REPOSITORY_COLUMN_OWNER_ID + " = o." + OWNER_COLUMN_ID +
                " LIMIT " + String.valueOf(limit) + " OFFSET " + String.valueOf(offset);

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst())
            do {

                Owner owner = new Owner();
                owner.setId(cursor.getLong(cursor.getColumnIndex(OWNER_COLUMN_ID)));
                owner.setName(cursor.getString(cursor.getColumnIndex(OWNER_COLUMN_NAME)));
                owner.setPhoto(cursor.getString(cursor.getColumnIndex(OWNER_COLUMN_PHOTO)));

                Repository repo = new Repository();
                repo.setOwner(owner);

                repo.setId(cursor.getLong(cursor.getColumnIndex(REPOSITORY_COLUMN_ID)));
                repo.setName(cursor.getString(cursor.getColumnIndex(REPOSITORY_COLUMN_NAME)));
                repo.setDescription(cursor.getString(cursor.getColumnIndex(REPOSITORY_COLUMN_DESCRIPTION)));
                repo.setForks(cursor.getLong(cursor.getColumnIndex(REPOSITORY_COLUMN_FORKS)));
                repo.setStars(cursor.getLong(cursor.getColumnIndex(REPOSITORY_COLUMN_STARTS)));

                repositories.add(repo);
            } while (cursor.moveToNext());

        if (!cursor.isClosed())
            cursor.close();

        return repositories;
    }

    public void saveOwner(Owner owner) {
        try {
            database.insertOrThrow(OWNER_TABLE_NAME, null, owner.values());
        } catch (SQLiteConstraintException cex) {
            database.update(OWNER_TABLE_NAME, owner.values(), OWNER_COLUMN_ID + " = ?", new String[]{String.valueOf(owner.getId())});
        } catch (Exception ex) {
            Log.e(TAG_DGE, "Error when insert or update Owner: " + ex.getMessage());
        }
    }

    public void saveRepository(Repository repository) {
        saveOwner(repository.getOwner());
        try {
            database.insertOrThrow(REPOSITORY_TABLE_NAME, null, repository.values());
        } catch (SQLiteConstraintException cex) {
            database.update(REPOSITORY_TABLE_NAME, repository.values(), REPOSITORY_COLUMN_ID + " = ?", new String[]{String.valueOf(repository.getId())});
        } catch (Exception ex) {
            Log.e(TAG_DGE, "Error when insert or update Repository: " + ex.getMessage());
        }
    }

    public void savePullRequest(PullRequest pullRequest) {
        saveOwner(pullRequest.getOwner());
        try {
            database.insertOrThrow(PULL_REQUEST_TABLE_NAME, null, pullRequest.values());
        } catch (SQLiteConstraintException cex) {
            database.update(PULL_REQUEST_TABLE_NAME, pullRequest.values(), PULL_REQUEST_COLUMN_ID + " = ?", new String[]{String.valueOf(pullRequest.getId())});
        } catch (Exception ex) {
            Log.e(TAG_DGE, "Error when insert or update Pull Request: " + ex.getMessage());
        }
    }

    private class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(OWNER_CREATE_TABLE);
            db.execSQL(REPOSITORY_CREATE_TABLE);
            db.execSQL(PULL_REQUEST_CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            this.onCreate(db);
        }
    }
}
