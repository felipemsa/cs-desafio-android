package br.com.felipemsa.gitexplorer.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.felipemsa.gitexplorer.R;
import br.com.felipemsa.gitexplorer.helper.RoundedTransformation;
import br.com.felipemsa.gitexplorer.model.Repository;

/**
 * Created by felipealmeida on 25/10/15.
 */
public class RepositoryAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Repository> repositories;

    public RepositoryAdapter(Context context, ArrayList<Repository> listRepos) {
        mContext = context;
        repositories = listRepos;
    }

    @Override
    public int getCount() {
        return repositories.size();
    }

    @Override
    public Repository getItem(int position) {
        return repositories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null)
            view = LayoutInflater.from(mContext).inflate(R.layout.list_repo_item, null);

        TextView tvRepoName = (TextView) view.findViewById(R.id.tvRepoName);
        TextView tvRepoDescription = (TextView) view.findViewById(R.id.tvRepoDescription);
        TextView tvRepoForks = (TextView) view.findViewById(R.id.tvRepoForks);
        TextView tvRepoStars = (TextView) view.findViewById(R.id.tvRepoStars);
        ImageView ivOwnerPhoto = (ImageView) view.findViewById(R.id.ivOwnerPhoto);
        TextView tvOwnerName = (TextView) view.findViewById(R.id.tvOwnerName);

        Repository repo = getItem(position);

        tvRepoName.setText(repo.getName());
        tvRepoDescription.setText(repo.getDescription());
        tvRepoForks.setText(Html.fromHtml("<b>Forks: </b>" + repo.getForks()));
        tvRepoStars.setText(Html.fromHtml("<b>Stars: </b>" + repo.getStars()));
        tvOwnerName.setText(repo.getOwner().getName());

        Picasso.with(mContext).load(repo.getOwner().getPhoto()).transform(new RoundedTransformation(400, 5)).fit().into(ivOwnerPhoto);

        return view;
    }
}
