package br.com.felipemsa.gitexplorer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.felipemsa.gitexplorer.R;
import br.com.felipemsa.gitexplorer.adapter.RepositoryAdapter;
import br.com.felipemsa.gitexplorer.helper.EndlessScrollListener;
import br.com.felipemsa.gitexplorer.model.Repository;
import br.com.felipemsa.gitexplorer.repository.Git;
import br.com.felipemsa.gitexplorer.repository.RepositoryDatabase;
import cz.msebera.android.httpclient.Header;

/**
 * Created by felipealmeida on 24/10/15.
 */
public class MainActivity extends AppCompatActivity {

    private Git git = Git.getInstance();
    private Gson gson = new Gson();

    private ListView list;
    private RepositoryAdapter adapter;
    private ArrayList<Repository> repositories = new ArrayList();

    private RepositoryDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        db = new RepositoryDatabase(this);

        list = (ListView) findViewById(R.id.list);
        adapter = new RepositoryAdapter(this, repositories);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent iPulls = new Intent(MainActivity.this, PullRequestActivity.class);
                iPulls.putExtra("repository", gson.toJson(adapter.getItem(position)));
                startActivity(iPulls);
            }
        });

        list.setOnScrollListener(new EndlessScrollListener(10, 1) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                load(page, totalItemsCount);
                return true;
            }
        });

        load(1, 0);
    }

    private void load(int page, final int offset) {
        git.getRepositoryList(page, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray array = response.getJSONArray("items");

                    for (int i = 0; i < array.length(); i++) {
                        Repository repo = gson.fromJson(array.getJSONObject(i).toString(), Repository.class);
                        db.saveRepository(repo);
                    }

                    repositories.addAll(db.getRepositories(30, offset));

                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, "Ops!\nAlgo não está funcionando bem...\nTente novamente em alguns segundos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.d("ERROR", "statusCode: " + statusCode
                        + "\nheaders: " + headers
                        + "\nresponse: " + errorResponse);

                ArrayList<Repository> repos = db.getRepositories(30, offset);
                if (repos.size() > 0) {
                    repositories.addAll(repos);

                    adapter.notifyDataSetChanged();
                } else
                    Toast.makeText(MainActivity.this, "No momento não há mais itens a exibir...", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFinish() {
                findViewById(R.id.pd).setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }
}
