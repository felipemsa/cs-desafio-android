package br.com.felipemsa.gitexplorer.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import br.com.felipemsa.gitexplorer.repository.interfaces.GitRepositoriesInterface;

/**
 * Created by felipealmeida on 25/10/15.
 */
public class Owner {

    private long id;
    @SerializedName("login")
    private String name;
    @SerializedName("avatar_url")
    private String photo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ContentValues values() {
        ContentValues values = new ContentValues();

        values.put(GitRepositoriesInterface.OWNER_COLUMN_ID, getId());
        values.put(GitRepositoriesInterface.OWNER_COLUMN_NAME, getName());
        values.put(GitRepositoriesInterface.OWNER_COLUMN_PHOTO, getPhoto());

        return values;
    }
}
