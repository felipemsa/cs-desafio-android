package br.com.felipemsa.gitexplorer.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import br.com.felipemsa.gitexplorer.repository.interfaces.GitRepositoriesInterface;

/**
 * Created by felipealmeida on 25/10/15.
 */
public class PullRequest {

    private long id;
    private String title;
    private String body;
    @SerializedName("user")
    private Owner owner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public ContentValues values() {
        ContentValues values = new ContentValues();

        values.put(GitRepositoriesInterface.PULL_REQUEST_COLUMN_ID, getId());
        values.put(GitRepositoriesInterface.PULL_REQUEST_COLUMN_TITLE, getTitle());
        values.put(GitRepositoriesInterface.PULL_REQUEST_COLUMN_BODY, getBody());
        values.put(GitRepositoriesInterface.PULL_REQUEST_COLUMN_OWNER_ID, getOwner().getId());

        return values;
    }
}
